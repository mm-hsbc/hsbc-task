# HSBC iOS Technical Task

## The Brief:

We would like you to implement an image gallery application (iOS or Android) that uses the public image Flickr
feed as a data source.
Requirements

- Use the most recent tools for the platform of your choice (iOS – Swift/Xcode or Android - Java /Android
Studio or Android Java/Kotlin/Android Studio)
- Limit your usage of 3rd party libraries only to the few ones that add a large benefit to the architecture and
testability of the project.
- Flickr url that should be used is: https://www.flickr.com/services/feeds/docs/photos_public
- Image metadata should be visible for each picture
- Git should be used as version control and to track the application development

Optional

- Search for images by tag
- Image caching
- Order by date taken or date published
- Save image to the System Gallery
- Open image in system browser
- Share picture by email

Output

- Provide a link to the Github or Bitbucket repository with a project.
References
- JSON feed documentation: https://www.flickr.com/services/api/response.json.html

## Implementation

### Key points:

- VIPER architecture.
- Networking layer
- Unit Tests

#### Architecture:

The App architecture is based on VIPER and the 'Modules' are generated using [Generamba](https://github.com/rambler-digital-solutions/Generamba).
The generator will create all the VIPER component, in some cases not all of them are necessary and will remain empty, however I kept them in the project since will describe the VIPER module in its completeness and it will be ready for future changes whitout adding any other source file.

#### Networking layer

The networking layer represent the API client of the given service. The API client responses objects are designed to be high level and adjustable in the future, incapsulating the parsed entity in Result objects so the API client interface will never change if the response of specific endpoint change, reducing the code that need to be changed in the rest of the app, only the entities incapsulated will change (in number and structure).

#### Unit Tests

Few unit tests have been written for this project, they are there not to cover the whole project but just to give an indication of tests structure

## Thanks
