//
//  UIViewController+ErrorAlert.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

import UIKit

extension UIViewController {
    
    /**
     Show the given message inside an Error decorated Alert
     
     - parameter message: the given message to display
     */
    func showError(message :String) {
        
        let alertController = UIAlertController(title: NSLocalizedString("Alert_Error_Title", comment: ""),
                                                message: message,
                                                preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Alert_Error_Action", comment: ""),
                                         style: .cancel,
                                         handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
