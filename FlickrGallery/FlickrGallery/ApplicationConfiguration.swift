//
//  ApplicationConfiguration.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

class ApplicationConfiguration : NSObject {
    
    /**
     Application configuration constants.
     */
    enum ConfigurationConstant {
        
        static let apiHost = API_Host
        static let memoryCacheCapacity :Int = 20 * 1024 * 1024  // 20 MB in memory cache
        static let diskCacheCapacity :Int = 100 * 1024 * 1024   // 100 MB in memory cache
        static let cacheDiskFolder :String = "flickrImageCache"
        static let cachePolicy :NSURLRequest.CachePolicy = .reloadRevalidatingCacheData
    }
    
    lazy var apiService :FlickrAPIService =  {
        
        let imageCache = URLCache(memoryCapacity: ConfigurationConstant.memoryCacheCapacity,
                                  diskCapacity: ConfigurationConstant.diskCacheCapacity,
                                  diskPath: ConfigurationConstant.cacheDiskFolder)
        
        let apiConfiguration = FlickrAPIConfiguration(host: ConfigurationConstant.apiHost,
                                                      imageCache: imageCache,
                                                      cachePolicy: ConfigurationConstant.cachePolicy)
        
        return FlickrAPIClient(with: apiConfiguration)
    }()
}
