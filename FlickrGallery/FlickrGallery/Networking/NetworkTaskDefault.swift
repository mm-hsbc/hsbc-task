//
//  NetworkTaskDefault.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

/*
 Default protocol implementaion, valid for general cases
 */
extension NetworkTask {
    
    func perform(completion: @escaping (NetworkTaskResult<Model>) -> Void) {
        
        sessionTask = session.dataTask(with: request) { [weak self] (data :Data?, response :URLResponse?, requestError :Error?) in
            
            if let result = self?.handle(data: data, response: response, error: requestError) {
                completion(result)
            } else {
                completion((nil, NetworkTaskError.unkwonError))
            }
        }
        
        sessionTask?.resume()
    }
    
    func cancel() {
        sessionTask?.cancel()
    }
    
    func handle(data :Data?, response :URLResponse?, error :Error?) -> NetworkTaskResult<Model> {
        
        var result :NetworkTaskResult<Model> = (nil, nil)
        
        if let httpResponse = response as? HTTPURLResponse {
            
            if isSuccessCode(httpResponse.statusCode) == false {
                result.error = NetworkTaskError.statusCode(code: httpResponse.statusCode)
            } else {
                result = parse(data: data)
            }
        }
        return result
    }
    
    func isSuccessCode(_ code :Int) -> Bool {
        return code == 200 // default success code
    }
}
