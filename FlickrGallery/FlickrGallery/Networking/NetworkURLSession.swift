//
//  APIURLSession.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 06/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

/**
 Mimic the URLSession interface
 */
public protocol NetworkURLSession {
    
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) -> URLSessionDataTask
    
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) -> URLSessionDataTask
}

extension URLSession: NetworkURLSession {}


