//
//  NetworkTask.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

/**
 NetworkTask error encapsulation
 */
enum NetworkTaskError :Error {
    case invalidResponse        // invalid response error
    case unkwonError            // uknown error
    case statusCode(code :Int)  // http status code error
}

/**
 Cancellable task
 - description: interface to control (cancel) a running task
 */
protocol CancellableTask {
    func cancel()
}

/**
 Touple representing the result status of a task
 */
typealias NetworkTaskResult<Model> = (object :Model?, error :Error?)

/**
 Network task interface
 */
protocol NetworkTask :class, CancellableTask {
    
    /**
     The expected result data model
    */
    associatedtype Model
    
    /**
     The session serving the task
     */
    var session     :NetworkURLSession { get set }
    
    /**
     The session task
     */
    var sessionTask :URLSessionTask? { get set }
    
    /**
     The URL request
     */
    var request     :URLRequest { get set }
    
    /**
     Initializer
     
     - parameter: session the NetworkURLSession that will execute the task
     - parameter: request the URL request to execute
     */
    init(session :NetworkURLSession, request :URLRequest)
    
    /**
     Method to start the execution of the task
     
     - parameter completion: a completion function called when the task completes returning as parameter a NetworkTaskResult with the result model
     */
    func perform(completion: @escaping ((NetworkTaskResult<Model>) -> Void))
    
    /**
     Method delegated to handle the response of the request
     
     - parameter data: the data fetched by the request if any
     - parameter response: the request URLResponse if any
     - parameter error: the request error if any
     
     - return: a NetworkTaskResult<Model> with the given data model
     */
    func handle(data :Data?, response :URLResponse?, error :Error?) -> NetworkTaskResult<Model>
    
    /**
     Method delegated to parse the responce data and convert to the result model
     
     - parameter data: the data to parse and convert to the result model
     
     - return: a NetworkTaskResult<Model> with the given data model
    */
    func parse(data :Data?) -> NetworkTaskResult<Model>
    
    /**
     Method to check the HTTP status code depending on the request expectations
     
     - parameter  code: the request status code
     
     - return: true if the code is successfull for the request expectations, false otherwise
    */
    func isSuccessCode(_ code :Int) -> Bool
}
