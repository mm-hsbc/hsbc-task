//
//  SpinnerViewController.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 06/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class SpinnerViewController: UIViewController {
    
    let animationView: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        
        animationView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(animationView)
        
        let centerX = NSLayoutConstraint(item: animationView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        
        let centerY = NSLayoutConstraint(item: animationView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([centerX, centerY])
        
        animationView.startAnimating()
    }
    
    /**
     Start the SpinnerViewController over the give view controller
     
     - parameter viewController: the given view controller to display the spinner over
     */
    static func start(from viewController :UIViewController) -> SpinnerViewController {
        
        let spinnerViewController = SpinnerViewController()
        spinnerViewController.providesPresentationContextTransitionStyle = true
        spinnerViewController.definesPresentationContext = true
        spinnerViewController.modalPresentationStyle = .overCurrentContext
        
        viewController.present(spinnerViewController, animated: false, completion: nil)
        
        return spinnerViewController
    }
    
    /**
     Stop the SpinnerViewController
     */
    func stop() {
        
        self.dismiss(animated: false, completion: nil)
    }
}
