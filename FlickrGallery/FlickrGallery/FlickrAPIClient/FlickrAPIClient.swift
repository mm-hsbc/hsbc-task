//
//  APIClient.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 06/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation
import UIKit

enum FlickrAPIClientError :Error {
    
    case invalidConfiguration
    case invalidResource
}

/**
 Flickr API Service interface
 */
protocol FlickrAPIService {
 
    /**
     The Service configuration object
     */
    var configuration :FlickrAPIConfiguration { get set }
    
    /**
     GET request to retrieve the feed
     - parameter handler: the completion handler called on task completion, encapsulate (FeedResult, Error)
    */
    func feed(handler: @escaping (NetworkTaskResult<FeedResult>) -> Void) throws -> CancellableTask
    
    /**
     GET request to retrieve the media for the given URL
     - parameter handler: the completion handler called on task completion, encapsulate (UIImage, Error)
     */
    func image(media: String, handler: @escaping (NetworkTaskResult<UIImage>) -> Void) throws -> CancellableTask
}

class FlickrAPIClient : FlickrAPIService {
    
    var configuration :FlickrAPIConfiguration
    
    lazy var imageSession: NetworkURLSession = {
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.urlCache = configuration.imageCache
        sessionConfig.requestCachePolicy = configuration.cachePolicy
        return URLSession(configuration: sessionConfig)
    }()
    
    var jsonAPISession : NetworkURLSession = URLSession(configuration: URLSessionConfiguration.ephemeral)
    
    init(with configuration :FlickrAPIConfiguration) {
        
        self.configuration = configuration
    }
    
    private lazy var requestBuilder :FlickrAPIRequestBuilder?  = {
        
        guard let requestBaseUrl = URL(string: configuration.host) else {
            return nil
        }
        
        return  FlickrAPIRequestBuilder(requestBaseUrl)
    }()
    
    func feed(handler: @escaping (NetworkTaskResult<FeedResult>) -> Void) throws -> CancellableTask {
    
        guard let builder = requestBuilder else {
            throw FlickrAPIClientError.invalidConfiguration
        }
        
        let feedTask = FlickrAPITaskFeed(session: jsonAPISession, builder: builder)
        
        feedTask.perform(completion: handler)
        
        return feedTask
    }
    
    func image(media: String, handler: @escaping (NetworkTaskResult<UIImage>) -> Void) throws -> CancellableTask {
        
        guard let mediaUrl = URL(string: media) else {
            throw FlickrAPIClientError.invalidResource
        }
        
        let imageTask = FlickrAPITaskImage(session: imageSession, url: mediaUrl)
        
        imageTask.perform(completion: handler)
        
        return imageTask
    }
}
