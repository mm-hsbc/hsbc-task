//
//  APIRequestBuilder.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 06/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

class FlickrAPIRequestBuilder {
    
    fileprivate enum Headers {
        static let ContentType           = "Content-Type"
        static let ContentTypeJSON       = "application/json"
    }
    
    static private let timeout = 30.0
    
    private let baseURL :URL
    
    /**
     URLRequest builder
     create and configure request for the API
     */
    init(_ baseURL :URL) {
        self.baseURL = baseURL
    }
    
    /**
     Build and return a URLRequest for the given endpoint, http method, body and headers
     
     - parameter path: the path relative to the base host
     - parameter resource: the resource name relative to the path
     - parameter method: the request HTTP method (GET, POST, etc...)
     - parameter parameters: a dictionary with the request query parameters
     - parameter body: the HTTP body for the request
     
     - return: the request configured with the given parameters
     */
    func build(_ path :String?, _ resource :String?, _ method :String, _ parameters :[String:String]?, _ body :Data?) -> URLRequest {
        
        var url = self.baseURL
        
        if let requestPath = path {
            url = url.appendingPathComponent(requestPath)
        }
        
        if let requestResource = resource {
            url = url.appendingPathComponent(requestResource)
        }
        
        let queryItems :Array<URLQueryItem>? = parameters?.map {
            URLQueryItem(name: $0, value: $1)
        }
        
        let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: false)
        urlComponents?.queryItems = queryItems
        
        var request = URLRequest(url: urlComponents?.url ?? url,
                                 cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: FlickrAPIRequestBuilder.timeout)
        
        request.httpMethod = method
        request.httpBody = body
        
        // set base API headers
        let baseHeaders = [Headers.ContentType : Headers.ContentTypeJSON]
        
        request.addHTTPHeaders(headers: baseHeaders)
        
        return request
    }
}

// add all headrers in one go
private extension URLRequest {
    
    mutating func addHTTPHeaders(headers :Dictionary<String, String>) {
        
        for (header, value) in headers {
            
            self.addValue(value, forHTTPHeaderField: header)
        }
    }
}
