//
//  JSONModel.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 06/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

/**
 JSONModel error encapsulation
 */
enum JSONModelError : Error {
    case invalidContetnForParameter(parameter :String)
    case missingResponseParameter(parameter :String)
}

/**
 Result model protocol
 */
protocol JSONModel {
    
    /**
     Model initializer
     - parameter jsonDictionary: JSON dictionary used to populate the model
    */
    init(with jsonDictionary :[String :Any]) throws
}
