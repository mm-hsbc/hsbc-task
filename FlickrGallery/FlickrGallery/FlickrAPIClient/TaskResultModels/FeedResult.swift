//
//  FeedResult.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 06/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

struct FeedResult :JSONModel {
   
    let feed :FlickrFeed
    
    init(with jsonDictionary: [String : Any]) throws {
        
        feed = try FlickrFeed(with: jsonDictionary)
    }
}
