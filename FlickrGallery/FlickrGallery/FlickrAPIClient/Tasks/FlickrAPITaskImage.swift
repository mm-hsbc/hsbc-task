//
//  APITaskImage.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

/**
 Flicker image task implementation
 */
class FlickrAPITaskImage :NetworkTask {
    
    typealias Model = UIImage

    var session: NetworkURLSession
    var sessionTask: URLSessionTask?
    var request: URLRequest
    
    required init(session: NetworkURLSession, request: URLRequest) {
        
        self.session = session
        self.request = request
    }
    
    convenience init(session :NetworkURLSession, url :URL) {
        
        let request = URLRequest(url: url)
        
        self.init(session: session, request: request)
    }
    
    func parse(data: Data?) -> (object: UIImage?, error: Error?) {
     
        var image :UIImage?
        var taskError :Error?
        
        if let resultData = data  {
            image = UIImage(data: resultData)
        }
        
        if image == nil {
            taskError = NetworkTaskError.invalidResponse
        }
        
        return (image, taskError)
    }
}
