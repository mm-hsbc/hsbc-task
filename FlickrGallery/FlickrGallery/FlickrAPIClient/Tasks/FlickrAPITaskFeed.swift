//
//  APITaskFeed.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 06/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

/**
 Flicker feed task implementation
 */
class FlickrAPITaskFeed :FlickrAPIJSONTask<FeedResult> {
    
    enum FlickrAPI {
        
        static let formatQueryParamter      = "format"
        static let formatQueryValue         = "json"
        static let callbackQueryParameter   = "nojsoncallback"
        static let callbackQueryValue       = "1"
        
        static let feedsPath                = "services/feeds"
        static let publicFeedResource       = "photos_public.gne"
    }
    
    convenience init(session :NetworkURLSession, builder :FlickrAPIRequestBuilder) {
        
        let parameters :[String : String] = [FlickrAPI.formatQueryParamter : FlickrAPI.formatQueryValue,
                                             FlickrAPI.callbackQueryParameter : FlickrAPI.callbackQueryValue]
        
        let body = try? JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        let request = builder.build(FlickrAPI.feedsPath, FlickrAPI.publicFeedResource, "GET", parameters, body)
        
        self.init(session: session, request: request)
    }
}
