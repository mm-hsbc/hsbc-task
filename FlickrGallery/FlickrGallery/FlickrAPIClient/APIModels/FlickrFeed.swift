//
//  Feed.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 06/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

/**
 Flickr Feed data model
 */
struct FlickrFeed {

    let title: String           // Feed title
    let items:[FlickrFeedItem]  // Feed items
}

/**
 JSONModel externsion for FlickrFeed
 */
extension FlickrFeed :JSONModel {
    
    enum JsonKey {
        static let title = "title"
        static let items = "items"
    }
    
    init(with jsonDictionary: [String : Any]) throws {
        
        guard let jsonTitle = jsonDictionary[JsonKey.title] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.title)
        }
        self.title = jsonTitle
        
        guard let jsonItems = jsonDictionary[JsonKey.items] as? Array<[String : Any]> else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.items)
        }
        
        self.items = try jsonItems.map {
            try FlickrFeedItem(with: $0)
        }
    }
}
