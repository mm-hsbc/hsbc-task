//
//  FlickrFeedItem.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

/**
 Flickr Feed Item data model
 */
public struct FlickrFeedItem {
    
    let title: String
    let link: String
    let media: [String:String]
    let dateTaken: Date
    let description: String
    let published: String
    let author: String
    let authorId: String
    let tags: String
}

extension FlickrFeedItem :Hashable {
    
    public var hashValue: Int {
        
        return "\(title) \(authorId) \(dateTaken)".hashValue
    }
    
    public static func ==(lhs: FlickrFeedItem, rhs: FlickrFeedItem) -> Bool {
        
        return lhs.hashValue == rhs.hashValue
    }
}

/**
 JSONModel externsion for FlickrFeedItem
 */
extension FlickrFeedItem  : JSONModel {
    
    enum FlickAPI {
        static let dateFormat = "yyyy-MM-DD'T'HH:mm:ssXXX"
    }
    
    enum JsonKey {
        static let title        = "title"
        static let link         = "link"
        static let media        = "media"
        static let dateTaken    = "date_taken"
        static let description  = "description"
        static let published    = "published"
        static let author       = "author"
        static let authorId     = "author_id"
        static let tags         = "tags"
    }
    
    init(with jsonDictionary: [String : Any]) throws {
        
        guard let title = jsonDictionary[JsonKey.title] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.title)
        }
        self.title = title
        
        guard let link = jsonDictionary[JsonKey.link] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.link)
        }
        self.link = link
        
        guard let media = jsonDictionary[JsonKey.media] as? [String:String] else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.media)
        }
        self.media = media
        
        guard let dateString = jsonDictionary[JsonKey.dateTaken] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.dateTaken)
        }
        guard let dateTaken = FlickrFeedItem.dateFromString(dateString) else {
            throw JSONModelError.invalidContetnForParameter(parameter: JsonKey.dateTaken)
        }
        self.dateTaken = dateTaken
        
        guard let description = jsonDictionary[JsonKey.description] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.description)
        }
        self.description = description
        
        guard let published = jsonDictionary[JsonKey.published] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.published)
        }
        self.published = published
        
        guard let author = jsonDictionary[JsonKey.author] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.author)
        }
        self.author = author
        
        guard let authorId = jsonDictionary[JsonKey.authorId] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.authorId)
        }
        self.authorId = authorId
        
        guard let tags = jsonDictionary[JsonKey.tags] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: JsonKey.tags)
        }
        self.tags = tags
    }
}

fileprivate extension FlickrFeedItem {
    
    static func dateFromString(_ string :String) -> Date? {
        
        let formatter = DateFormatter()
        formatter.dateFormat = FlickAPI.dateFormat
        return formatter.date(from: string)
    }
}
