//
//  FlickrAPIConfiguration.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

/**
 Structure encapsulating the FlickrAPI configuration
 */
struct FlickrAPIConfiguration {
    
    let host :String                            // the API host
    let imageCache :URLCache                    // the URLCache object for the images
    let cachePolicy: NSURLRequest.CachePolicy   // Images cache policy
}
