//
//  APIJSONTask.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 06/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

/**
 Generic implementaion of NetworkTask to handle JSONModel as result
 */
class  FlickrAPIJSONTask<Result> :NetworkTask where Result:JSONModel {

    typealias Model = Result

    var session: NetworkURLSession
    var sessionTask :URLSessionTask?
    var request :URLRequest
    
    required init(session :NetworkURLSession, request :URLRequest) {
        
        self.session = session
        self.request = request
    }
    
    func parse(data :Data?) -> NetworkTaskResult<Result> {
        
        var model :Result?
        var taskError :Error?
        
        if let resultData = data  {
            
            let jsonObject = try? JSONSerialization.jsonObject(with: resultData, options: [])
            
            if let dictionary = jsonObject as? [String: Any] {
                
                do {
                    model = try Result(with: dictionary)
                } catch {
                    taskError = error
                }
            } else {
                taskError = NetworkTaskError.invalidResponse
            }
        }
        return (model, taskError)
    }
}
