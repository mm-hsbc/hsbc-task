//
//  FeedItemDetailsFeedItemDetailsConfigurator.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedItemDetailsModuleConfigurator {
    
    var item :FlickrFeedItem?
    var apiService :FlickrAPIService?

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? FeedItemDetailsViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: FeedItemDetailsViewController) {

        let router = FeedItemDetailsRouter()
        router.viewController = viewController
        
        let presenter = FeedItemDetailsPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.item = item

        let interactor = FeedItemDetailsInteractor()
        interactor.output = presenter
        interactor.apiService = apiService

        presenter.interactor = interactor
        viewController.output = presenter
    }
}
