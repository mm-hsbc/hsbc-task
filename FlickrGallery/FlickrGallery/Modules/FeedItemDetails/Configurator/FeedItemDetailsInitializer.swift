//
//  FeedItemDetailsFeedItemDetailsInitializer.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedItemDetailsModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var feeditemdetailsViewController: FeedItemDetailsViewController!

    override func awakeFromNib() {

        let configurator = FeedItemDetailsModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: feeditemdetailsViewController)
    }

}
