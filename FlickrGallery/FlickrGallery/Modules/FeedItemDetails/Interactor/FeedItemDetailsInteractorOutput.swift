//
//  FeedItemDetailsFeedItemDetailsInteractorOutput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

protocol FeedItemDetailsInteractorOutput: class {

    func imageLoaded(_ image :UIImage)
    
    func imageLoadingFailed(with error :Error?)
}
