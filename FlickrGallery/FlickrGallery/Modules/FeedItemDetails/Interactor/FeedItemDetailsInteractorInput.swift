//
//  FeedItemDetailsFeedItemDetailsInteractorInput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

protocol FeedItemDetailsInteractorInput {

    var apiService :FlickrAPIService! { get set }

    func loadMedia(_ item :FlickrFeedItem)    
}
