//
//  FeedItemDetailsFeedItemDetailsInteractor.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedItemDetailsInteractor: FeedItemDetailsInteractorInput {
    
    weak var output: FeedItemDetailsInteractorOutput!
    
    var apiService :FlickrAPIService!
    
    var imageTask :CancellableTask?
    
    deinit {
        imageTask?.cancel()
    }
    
    //MARK: FeedItemDetailsInteractorInput
    
    func loadMedia(_ item :FlickrFeedItem) {
        
        guard imageTask == nil, let imageLink = item.media.first?.value else {
            self.output.imageLoadingFailed(with: nil)
            return
        }
        
        do {
            imageTask = try apiService.image(media: imageLink) { [weak self] (result :NetworkTaskResult<UIImage>) in
                
                self?.imageTask = nil
                
                guard let image = result.object else {
                    self?.output.imageLoadingFailed(with: result.error)
                    return
                }
                
                self?.output.imageLoaded(image)
            }
        } catch {
            self.output.imageLoadingFailed(with: error)
        }
    }
}
