//
//  FeedItemDetailsFeedItemDetailsViewController.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedItemDetailsViewController: UIViewController, FeedItemDetailsViewInput {

    @IBOutlet weak var mediaImageView: UIImageView!
    var output: FeedItemDetailsViewOutput!
    weak var spinnerVieController :SpinnerViewController?


    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    // MARK: - IBAction
    
    @IBAction func shareButtonTapped(_ sender: UIBarButtonItem) {
        output.didTapShareButton()
    }
    
    @IBAction func browserButtonTapped(_ sender: UIBarButtonItem) {
        output.didTapBrowserButton()
    }
    
    // MARK: - FeedItemDetailsViewInput
    func setupInitialState() {
        
        self.title = output.itemTitle()
    }
    
    func showImage(_ image: UIImage) {
        
        mediaImageView.image = image
    }
    
    func showError(_ error: Error) {
        
        self.showError(message: error.localizedDescription)
    }
    
    func showActivityIndicator() {
        
        guard spinnerVieController == nil else {
            return
        }
        
        spinnerVieController = SpinnerViewController.start(from: self)
    }
    
    func hideActivityIndicator() {
        
        self.spinnerVieController?.stop()
    }
}
