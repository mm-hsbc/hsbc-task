//
//  FeedItemDetailsFeedItemDetailsViewOutput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

protocol FeedItemDetailsViewOutput {

    /**
        @author Manuele Maggi
        Notify presenter that view is ready
    */

    func viewIsReady()
        
    func itemTitle() -> String?
    
    func didTapShareButton()
    
    func didTapBrowserButton()
}
