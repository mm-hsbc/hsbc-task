//
//  FeedItemDetailsFeedItemDetailsViewInput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

protocol FeedItemDetailsViewInput: class {

    /**
        @author Manuele Maggi
        Setup initial state of the view
    */

    func setupInitialState()
    
    func showImage(_ image :UIImage)
    
    func showError(_ error :Error)
    
    func showActivityIndicator()
    
    func hideActivityIndicator()
}
