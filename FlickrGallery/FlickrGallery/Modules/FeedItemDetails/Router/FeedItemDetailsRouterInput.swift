//
//  FeedItemDetailsFeedItemDetailsRouterInput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

protocol FeedItemDetailsRouterInput {

    weak var viewController :UIViewController? { get set }

    func openItemInBrowser(_ item: FlickrFeedItem)
    
    func share(item: FlickrFeedItem)
}
