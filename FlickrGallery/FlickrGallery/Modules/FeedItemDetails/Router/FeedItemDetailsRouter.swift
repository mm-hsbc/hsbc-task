//
//  FeedItemDetailsFeedItemDetailsRouter.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedItemDetailsRouter: FeedItemDetailsRouterInput {

    weak var viewController :UIViewController?
    
    func openItemInBrowser(_ item: FlickrFeedItem) {
        
        guard let url = URL(string: item.link) else {
            return
        }
        
        UIApplication.shared.open(url,
                                  options: [:],
                                  completionHandler: nil)
    }
    
    func share(item: FlickrFeedItem) {
        
        
        let shareViewController = UIActivityViewController(activityItems: [item.link], applicationActivities: nil)
        shareViewController.excludedActivityTypes = [.mail]
        shareViewController.setValue(item.title, forKey: "subject")
        
        viewController?.present(shareViewController, animated: true, completion: nil)
    }
}
