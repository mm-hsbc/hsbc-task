//
//  FeedItemDetailsFeedItemDetailsModuleInput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

protocol FeedItemDetailsModuleInput: class {

    var item :FlickrFeedItem? { get set }
}
