//
//  FeedItemDetailsFeedItemDetailsPresenter.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedItemDetailsPresenter: FeedItemDetailsModuleInput, FeedItemDetailsViewOutput, FeedItemDetailsInteractorOutput {
    
    weak var view: FeedItemDetailsViewInput!
    var interactor: FeedItemDetailsInteractorInput!
    var router: FeedItemDetailsRouterInput!
    
    var item :FlickrFeedItem?
    
    // MARK: - FeedItemDetailsInteractorOutput
    
    func imageLoaded(_ image: UIImage) {
        
        DispatchQueue.main.async { [unowned self] in
            self.view.hideActivityIndicator()
            self.view.showImage(image)
        }
    }
    
    func imageLoadingFailed(with error: Error?) {
        
        
        var errorDescription :String!
        var errorCode = 0
        
        switch error as? NetworkTaskError {
            
        case .invalidResponse?:
            errorDescription = NSLocalizedString("Item_Details_Error_Invalid_Content", comment: "")
            break
            
        case .statusCode(let code)?:
            errorCode = code
            errorDescription = NSLocalizedString("Item_Details_Error_With_Code", comment: "") + "\(code)"
            break
            
        case .unkwonError?:
            fallthrough
        default:
            errorDescription = NSLocalizedString("Item_Details_Error_Uknown", comment: "")
            break
        }
        
        let localizedError = NSError(domain: String(describing: FeedItemDetailsPresenter.self),
                                     code: errorCode,
                                     userInfo: [NSLocalizedDescriptionKey : errorDescription])
        
        DispatchQueue.main.async { [unowned self] in
            self.view.hideActivityIndicator()
            self.view.showError(localizedError)
        }
    }
    
    // MARK: - FeedItemDetailsViewOutput
    
    func viewIsReady() {

        view.setupInitialState()
        
        guard let flickrItem = item else {
            
            let localizedError = NSError(domain: String(describing: FeedItemDetailsPresenter.self),
                                         code: 0,
                                         userInfo: [NSLocalizedDescriptionKey : NSLocalizedString("Item_Details_Error_Configuration", comment: "")])
            
            view.showError(localizedError)
            return
        }
        
        view.showActivityIndicator()
        interactor.loadMedia(flickrItem)
    }
    
    func itemTitle() -> String? {
        
        return item?.title
    }
    
    func didTapShareButton() {
        
        guard let flickrItem = item else {
            return
        }
        
        router.share(item: flickrItem)
        
    }
    
    func didTapBrowserButton() {
        
        guard let flickrItem = item else {
            return
        }
        
        router.openItemInBrowser(flickrItem)
    }
}
