//
//  FeedFeedRouterInput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

protocol FeedRouterInput {

    weak var navigationController :UINavigationController? { get set}
    
    func showFeedItemDetails(item :FlickrFeedItem, apiService :FlickrAPIService)
}
