//
//  FeedFeedRouter.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedRouter: FeedRouterInput {

    weak var navigationController :UINavigationController?
    
    func showFeedItemDetails(item :FlickrFeedItem, apiService :FlickrAPIService) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let feedItemDetailsViewController = storyboard.instantiateViewController(withIdentifier: String(describing: FeedItemDetailsViewController.self)) as? FeedItemDetailsViewController else {
            return
        }
        
        let configurator = FeedItemDetailsModuleConfigurator()
        configurator.item = item
        configurator.apiService = apiService
        configurator.configureModuleForViewInput(viewInput: feedItemDetailsViewController)
        
        self.navigationController?.pushViewController(feedItemDetailsViewController, animated: true)
    }
}
