//
//  FeedFeedInteractorOutput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

protocol FeedInteractorOutput: class {

    func feedLoaded()
    func feedFailed(with error: Error?)
    
    func image(_ image :UIImage, loadedFor element :FlickrFeedItem)
}
