//
//  FeedFeedInteractorInput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import Foundation

protocol FeedInteractorInput {
    
    var apiService :FlickrAPIService! { get set }
    
    var feed :FlickrFeed? { get set }
    
    func loadFeed()
    
    func loadMediaForFeedItem(_ item :FlickrFeedItem)
    
    func cancelMediaLoadingForFeedItem(_ item :FlickrFeedItem)
}
