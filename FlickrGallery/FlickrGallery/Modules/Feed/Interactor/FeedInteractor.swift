//
//  FeedFeedInteractor.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedInteractor: FeedInteractorInput {

    weak var output: FeedInteractorOutput!

    var apiService :FlickrAPIService!
    
    var feedTask :CancellableTask?
    
    var imageTasks = [FlickrFeedItem : CancellableTask]()

    // MARK: FeedInteractorInput
    
    var feed: FlickrFeed?
    
    func loadFeed() {
        
        feedTask?.cancel()
        
        do {
            feedTask = try apiService.feed { [weak self] (result :NetworkTaskResult<FeedResult>) in
                
                guard let feed = result.object?.feed else {
                    self?.output.feedFailed(with: result.error)
                    return
                }
                
                self?.feed = feed
                self?.output.feedLoaded()
            }
        } catch {
            output.feedFailed(with: error)
        }
    }
    
    func loadMediaForFeedItem(_ item :FlickrFeedItem) {
        
        guard imageTasks[item] == nil, let imageLink = item.media.first?.value else {
            return
        }
        
        let imageTask = try? apiService.image(media: imageLink) { [weak self] (result :NetworkTaskResult<UIImage>) in
            
            self?.removeImageTask(for: item)
            
            guard let image = result.object else {
                return
            }
            
            self?.output.image(image, loadedFor: item)
        }
        
        if let task = imageTask {
            self.addImageTask(task, for: item)
        }
    }
    
    func cancelMediaLoadingForFeedItem(_ item :FlickrFeedItem) {
        
        imageTasks.removeValue(forKey: item)?.cancel()
    }
    
    // MARK: - Private methods
    
    /*  This is a very simple way to sych up the access to the imageTasks array, can also be done with GCD
        using semaphores with an concurrent queue or a simply with serial queue 
     */
    var lock = NSRecursiveLock()
    
    func addImageTask(_ task :CancellableTask, for item :FlickrFeedItem) {
        
        lock.lock()
        imageTasks[item] = task
        lock.unlock()
    }
    
    func removeImageTask(for item :FlickrFeedItem) {
        
        lock.lock()
        imageTasks.removeValue(forKey: item)
        lock.unlock()
    }
}
