//
//  FeedFeedPresenter.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedPresenter: FeedModuleInput, FeedViewOutput, FeedInteractorOutput {
    
    weak var view: FeedViewInput!
    var interactor: FeedInteractorInput!
    var router: FeedRouterInput!
    
    lazy var dateFormatter :DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.locale = Locale.current
        
        return formatter
    }()

    // MARK: - FeedViewOutput
    
    func viewIsReady() {
        
        view.setupInitialState()
        loadFeed()
    }
    
    func feedTitle() -> String {
        
        return (interactor.feed?.title ?? NSLocalizedString("Feed_Title_Loading", comment: "")).localizedCapitalized
    }
    
    func refreshActioned() {
        loadFeed()
    }
    
    func canShowFeed() -> Bool {
        
        return interactor.feed != nil
    }
    
    func numberOfElementsInFeed() -> Int {
        
        return interactor.feed?.items.count ?? 0
    }
    
    func titleForElementAt(index: Int) -> String? {
        
        return interactor.feed?.items[index].title
    }
    
    func dateTakenForElementAt(index: Int) -> String? {
        
        guard let date = interactor.feed?.items[index].dateTaken else {
            return nil
        }
        
        return dateFormatter.string(from: date)
    }
    
    func tagsForElementAt(index: Int) -> String? {
        
        guard let element = interactor.feed?.items[index] else {
            return nil
        }
        
        return element.tags.components(separatedBy: " ")
            .flatMap { $0.count > 0 ? "#\($0)" : nil }
            .joined(separator: " ")
    }
    
    func willDisplayElementAt(index: Int) {
        
        guard let element = interactor.feed?.items[index] else {
            return
        }
        
        interactor.loadMediaForFeedItem(element)
    }
    
    func didHideElementAt(index: Int) {
        
        guard let element = interactor.feed?.items[index] else {
            return
        }
        
        interactor.cancelMediaLoadingForFeedItem(element)
    }
    
    func didTapElementAt(index: Int) {
        
        guard let element = interactor.feed?.items[index] else {
            return
        }
        
        router.showFeedItemDetails(item: element, apiService: interactor.apiService)
    }
    
    // MARK: - FeedInteractorOutput
    
    func feedLoaded() {
        
        DispatchQueue.main.async { [unowned self] in
            self.view.hideActivityIndicator()
            self.view.reloadFeed()
        }
    }
    
    func feedFailed(with error: Error?) {
        
        var errorDescription :String!
        var errorCode = 0
        
        switch error as? NetworkTaskError {
            
        case .invalidResponse?:
            errorDescription = NSLocalizedString("Feed_Error_Invalid_Content", comment: "")
            break
        
        case .statusCode(let code)?:
            errorCode = code
            errorDescription = NSLocalizedString("Feed_Error_With_Code", comment: "") + "\(code)"
            break
            
        case .unkwonError?:
            fallthrough
        default:
            errorDescription = NSLocalizedString("Feed_Error_Uknown", comment: "")
            break
        }
        
        let localizedError = NSError(domain: String(describing: FeedPresenter.self),
                                     code: errorCode,
                                     userInfo: [NSLocalizedDescriptionKey : errorDescription])
        
        DispatchQueue.main.async { [unowned self] in
            self.view.hideActivityIndicator()
            self.view.showError(localizedError)
        }
    }
    
    func image(_ image :UIImage, loadedFor element :FlickrFeedItem) {
        
        guard let index = interactor.feed?.items.index(of: element) else {
            return
        }
        
        DispatchQueue.main.async { [unowned self] in
            self.view.showImage(image, at: index)
        }
    }
    
    // MARK: - Private methods
    
    private func loadFeed() {
        
        view.showActivityIndicator()
        interactor.loadFeed()
    }
}
