//
//  FeedFeedViewInput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

protocol FeedViewInput: class {

    /**
     Setup initial state of the view
    */
    func setupInitialState()
    
    /**
     Reload the Feed
     */
    func reloadFeed()
    
    /**
     Show the image for the item at the given index
     
     - parameter image: the image to show
     - parameter index: the index of the FeedItem related to the image to display
     */
    func showImage(_ image :UIImage, at index :Int)
    
    /**
     Show the error coming from the presenter
     
     - parameter error: an Error
     */
    func showError(_ error :Error)
    
    /**
     Show the activity indicator over the content
     */
    func showActivityIndicator()
    
    /**
     Remove the activity indicator
     */
    func hideActivityIndicator()
}
