//
//  FeedFeedViewController.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedViewController: UITableViewController, FeedViewInput {
    
    var output: FeedViewOutput!
    
    weak var spinnerVieController :SpinnerViewController?

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension

        self.refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshActioned), for: .valueChanged)
        
        output.viewIsReady()
    }

    // MARK: FeedViewInput
    func setupInitialState() {
        
        self.title = output.feedTitle()
    }
    
    func reloadFeed() {
        
        self.title = output.feedTitle()
        self.tableView.reloadData()
    }
    
    func showImage(_ image :UIImage, at index :Int) {
        
        guard let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? FeedItemTableViewCell else {
            return
        }
        
        cell.mediaImageView?.image = image
    }
    
    func showError(_ error: Error) {
        
        showError(message: error.localizedDescription)
    }
    
    func showActivityIndicator() {
        
        guard spinnerVieController == nil else {
            return
        }
        
        spinnerVieController = SpinnerViewController.start(from: self)
    }
    
    func hideActivityIndicator() {
        
        self.spinnerVieController?.stop()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
            self?.refreshControl?.endRefreshing()
        }
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return output.canShowFeed() ? 1 : 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return output.numberOfElementsInFeed()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedItemTableViewCell.self),
                                                 for: indexPath) as! FeedItemTableViewCell
        
        cell.mediaImageView.image = nil
        cell.titleLabel?.text = output.titleForElementAt(index: indexPath.row)
        cell.dateLabel?.text = output.dateTakenForElementAt(index: indexPath.row)
        cell.tagsLabel?.text = output.tagsForElementAt(index: indexPath.row)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        output.willDisplayElementAt(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        output.didHideElementAt(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        output.didTapElementAt(index: indexPath.row)
        tableView.reloadData()
    }
    
    // MARK: - Private methods
    
    @objc func refreshActioned() {
        output.refreshActioned()
    }
}
