//
//  FeedFeedViewOutput.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

protocol FeedViewOutput {

    /**
        @author Manuele Maggi
        Notify presenter that view is ready
    */

    func viewIsReady()
    
    func feedTitle() -> String
    
    func refreshActioned()
    
    func canShowFeed() -> Bool
    
    func willDisplayElementAt(index :Int)
    
    func didHideElementAt(index :Int)
    
    func numberOfElementsInFeed() -> Int
    
    func titleForElementAt(index :Int) -> String?
    
    func dateTakenForElementAt(index :Int) -> String?
        
    func tagsForElementAt(index: Int) -> String?
    
    func didTapElementAt(index: Int)
}
