//
//  FeedFeedConfigurator.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController, applicationConfiguration :ApplicationConfiguration) {

        if let viewController = viewInput as? FeedViewController {
            configure(viewController: viewController, applicationConfiguration: applicationConfiguration)
        }
    }

    private func configure(viewController: FeedViewController, applicationConfiguration :ApplicationConfiguration) {

        let router = FeedRouter()
        router.navigationController = viewController.navigationController

        let presenter = FeedPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = FeedInteractor()
        interactor.output = presenter
        interactor.apiService = applicationConfiguration.apiService

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
