//
//  FeedFeedInitializer.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import UIKit

class FeedModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var feedViewController: FeedViewController!
    @IBOutlet weak var applicationConfiguration :ApplicationConfiguration!

    override func awakeFromNib() {

        let configurator = FeedModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: feedViewController,
                                                 applicationConfiguration: applicationConfiguration)
    }

}
