//
//  FeedFeedInteractorTests.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FeedInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockPresenter: FeedInteractorOutput {
        
        func feedLoaded() {
            
        }
        
        func feedFailed(with error: Error?) {
            
        }
        
        func image(_ image: UIImage, loadedFor element: FlickrFeedItem) {
            
        }
    }
}
