//
//  FeedFeedPresenterTests.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 07/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FeedPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: FeedInteractorInput {
        var apiService: FlickrAPIService!
        
        var feed: FlickrFeed?
        
        func loadFeed() {
            
        }
        
        func loadMediaForFeedItem(_ item: FlickrFeedItem) {
            
        }
        
        func cancelMediaLoadingForFeedItem(_ item: FlickrFeedItem) {
            
        }
    }

    class MockRouter: FeedRouterInput {
        
        var navigationController: UINavigationController?
        
        func showFeedItemDetails(item: FlickrFeedItem, apiService: FlickrAPIService) {
            
        }
    }

    class MockViewController: FeedViewInput {
        
        func reloadFeed() {
            
        }
        
        func showImage(_ image: UIImage, at index: Int) {
            
        }
        
        func showError(_ error: Error) {
            
        }
        
        func showActivityIndicator() {
            
        }
        
        func hideActivityIndicator() {
            
        }
        
        func setupInitialState() {

        }
    }
}
