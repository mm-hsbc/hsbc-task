//
//  FeedItemDetailsFeedItemDetailsConfiguratorTests.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FeedItemDetailsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = FeedItemDetailsViewControllerMock()
        let configurator = FeedItemDetailsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "FeedItemDetailsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is FeedItemDetailsPresenter, "output is not FeedItemDetailsPresenter")

        let presenter: FeedItemDetailsPresenter = viewController.output as! FeedItemDetailsPresenter
        XCTAssertNotNil(presenter.view, "view in FeedItemDetailsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in FeedItemDetailsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is FeedItemDetailsRouter, "router is not FeedItemDetailsRouter")

        let interactor: FeedItemDetailsInteractor = presenter.interactor as! FeedItemDetailsInteractor
        XCTAssertNotNil(interactor.output, "output in FeedItemDetailsInteractor is nil after configuration")
    }

    class FeedItemDetailsViewControllerMock: FeedItemDetailsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
