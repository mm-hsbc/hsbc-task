//
//  FeedItemDetailsFeedItemDetailsPresenterTests.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FeedItemDetailsPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: FeedItemDetailsInteractorInput {
        
        var apiService: FlickrAPIService!
        
        func loadMedia(_ item: FlickrFeedItem) {
            
        }
    }

    class MockRouter: FeedItemDetailsRouterInput {
        var viewController: UIViewController?
        
        func openItemInBrowser(_ item: FlickrFeedItem) {
            
        }
        
        func share(item: FlickrFeedItem) {
            
        }
    }

    class MockViewController: FeedItemDetailsViewInput {
        
        func showImage(_ image: UIImage) {
            
        }
        
        func showError(_ error: Error) {
            
        }
        
        func showActivityIndicator() {
            
        }
        
        func hideActivityIndicator() {
            
        }
        
        func setupInitialState() {

        }
    }
}
