//
//  FeedItemDetailsFeedItemDetailsInteractorTests.swift
//  FlickrGallery
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FeedItemDetailsInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockPresenter: FeedItemDetailsInteractorOutput {
        
        func imageLoaded(_ image: UIImage) {
            
        }
        
        func imageLoadingFailed(with error: Error?) {
            
        }
    }
}
