//
//  FlickrAPIRequestBuilderTests.swift
//  FlickrGalleryTests
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FlickrAPIRequestBuilderTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBuild() {
        
        let flickrHost = "api.flickr.com"
        
        let builder = FlickrAPIRequestBuilder(URL(string: "https://" + flickrHost)!)
        
        let body = "body".data(using: .utf8)
        
        let parameters = [FlickrAPITaskFeed.FlickrAPI.formatQueryParamter : FlickrAPITaskFeed.FlickrAPI.formatQueryValue,
                          FlickrAPITaskFeed.FlickrAPI.callbackQueryParameter : FlickrAPITaskFeed.FlickrAPI.callbackQueryValue]
        
        let httpMethod = "GET"
        
        let request = builder.build(FlickrAPITaskFeed.FlickrAPI.feedsPath,
                                    FlickrAPITaskFeed.FlickrAPI.publicFeedResource,
                                    httpMethod,
                                    parameters,
                                    body)
        
        guard let url = request.url else {
            XCTFail("URL must not be nil")
            return
        }
        
        XCTAssertTrue(request.httpMethod == httpMethod, "http method doesn't match")
        XCTAssertTrue(request.httpBody == body, "http body doesn't match")
        XCTAssertTrue(url.host == flickrHost , "host doesn't match")
        XCTAssertTrue(url.relativePath == "/" + FlickrAPITaskFeed.FlickrAPI.feedsPath + "/" + FlickrAPITaskFeed.FlickrAPI.publicFeedResource , "host doesn't match")
        XCTAssertTrue(url.query == "\(FlickrAPITaskFeed.FlickrAPI.formatQueryParamter)=\(FlickrAPITaskFeed.FlickrAPI.formatQueryValue)&\(FlickrAPITaskFeed.FlickrAPI.callbackQueryParameter)=\(FlickrAPITaskFeed.FlickrAPI.callbackQueryValue)", "query doesn't match")
    }
}
