//
//  FlickrAPITaskImageTests.swift
//  FlickrGalleryTests
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FlickrAPITaskImageTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSucces() {
        
        let responseFileURL = Bundle(for: type(of: self)).url(forResource: "Image", withExtension: "jpg")
        let responseData = try? Data(contentsOf: responseFileURL!)
        
        let imageURL = URL(string: "https://farm1.staticflickr.com/787/41327127481_997d671c3e_m.jpg")!
        let urlResponse = HTTPURLResponse(url: imageURL,
                                          statusCode: 200,
                                          httpVersion: "HTTP/1.1",
                                          headerFields: nil)
        
        let mockSession = NetworkURLSessionMock(data: responseData,
                                                response: urlResponse,
                                                error: nil)
        
        let task = FlickrAPITaskImage(session: mockSession, url: imageURL)
        
        task.perform { (result :NetworkTaskResult<UIImage>) in
            
            XCTAssertNotNil(result.object, "Result object \"UIImage\" must not be nil")
            XCTAssertNil(result.error, "Result error must be nil")
        }
    }
    
    func testFailure() {
        
        let errorCode = 400
        
        let imageURL = URL(string: "https://farm1.staticflickr.com/787/41327127481_997d671c3e_m.jpg")!
        let urlResponse = HTTPURLResponse(url: imageURL,
                                          statusCode: errorCode,
                                          httpVersion: "HTTP/1.1",
                                          headerFields: nil)
        
        let mockSession = NetworkURLSessionMock(data: nil,
                                                response: urlResponse,
                                                error: nil)
        
        let task = FlickrAPITaskImage(session: mockSession, url: imageURL)
        
        task.perform { (result :NetworkTaskResult<UIImage>) in
            
            XCTAssertNil(result.object, "Result object must be nil")
            XCTAssertNotNil(result.error, "Result error must not be nil")
            
            guard let error = result.error as? NetworkTaskError else {
                XCTFail("Result error must be of type \"NetworkTaskError\"")
                return
            }
            
            guard case NetworkTaskError.statusCode(code: errorCode) = error else {
                XCTFail("error must be status code \(errorCode)")
                return
            }
        }
    }
}
