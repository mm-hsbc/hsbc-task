//
//  FlickrAPITaskFeedTests.swift
//  FlickrGalleryTests
//
//  Created by Manuele Maggi on 08/04/2018.
//  Copyright © 2018 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FlickrAPITaskFeedTests: XCTestCase {
    
    let builder = FlickrAPIRequestBuilder(URL(string: "https://api.flickr.com")!)
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSucces() {
        
        let responseFileURL = Bundle(for: type(of: self)).url(forResource: "Feed", withExtension: "json")
        let responseData = try? Data(contentsOf: responseFileURL!)
        
        let urlResponse = HTTPURLResponse(url: URL(string: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1")!,
                                          statusCode: 200,
                                          httpVersion: "HTTP/1.1",
                                          headerFields: nil)
        
        let mockSession = NetworkURLSessionMock(data: responseData,
                                                response: urlResponse,
                                                error: nil)
        
        let task = FlickrAPITaskFeed(session: mockSession, builder: builder)
        
        task.perform { (result :NetworkTaskResult<FeedResult>) in
            
            XCTAssertNotNil(result.object, "Result object must not be nil")
            
            guard let feed = result.object?.feed else {
                XCTFail("Result object \"Feed\" must not be nil")
                return
            }
            
            XCTAssertTrue(feed.items.count > 0, "")
            XCTAssertNil(result.error, "Result error must be nil")
        }
    }
    
    func testFailure() {
        
        let errorCode = 400
        
        let urlResponse = HTTPURLResponse(url: URL(string: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1")!,
                                          statusCode: errorCode,
                                          httpVersion: "HTTP/1.1",
                                          headerFields: nil)
        
        let mockSession = NetworkURLSessionMock(data: nil,
                                                response: urlResponse,
                                                error: nil)
        
        let task = FlickrAPITaskFeed(session: mockSession, builder: builder)
        
        task.perform { (result :NetworkTaskResult<FeedResult>) in
            
            XCTAssertNil(result.object, "Result object must be nil")
            XCTAssertNotNil(result.error, "Result error must not be nil")

            guard let error = result.error as? NetworkTaskError else {
                XCTFail("Result error must be of type \"NetworkTaskError\"")
                return
            }
            
            guard case NetworkTaskError.statusCode(code: errorCode) = error else {
                XCTFail("error must be status code \(errorCode)")
                return
            }
        }
    }
}
